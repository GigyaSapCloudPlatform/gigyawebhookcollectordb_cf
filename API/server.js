'use strict';


// ============================================

const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const xssec = require('@sap/xssec');
const JWTStrategy = require('@sap/xssec').JWTStrategy;
const xsenv = require('@sap/xsenv');

const dbConn = require('./lib/db');
const webhookController = require('./controllers/webhook');
const logger = require('./lib/logger');
var _db = undefined;

// ============================================
// Start the server
// ============================================

const app = express();
app.use(bodyParser.json());

//ENABLE EL JWT
//passport.use(new JWTStrategy(xsenv.getServices({uaa:{tag:'xsuaa'}}).uaa));
//app.use(passport.initialize());
//app.use(passport.authenticate('JWT', { session: false }));


// ROUTES
// ============================================

app.get("/", function (req, res) {
    res.status(200).send("WebHook Collector API");
});

app.get("/webhook", function (req, res) {
    // logJWT(req);
    webhookController.getAll(_db,res);
});

app.get("/webhook/:id", function (req, res) {

	//Check theuser have the scope
    /*if (!req.authInfo.checkLocalScope('Update')) {
        log('Missing the expected scope');
        res.status(403).end('Forbidden');
        return;
    }*/

    webhookController.getOne(_db, res, req.params.id);
});


// SERVER
// ============================================

function setDBCallback(error, db) {
    if (error !== null) {
        log('error when fetching the DB connection ' + JSON.stringify(error));
        return;
    }
    _db = db;
}

var PORT = process.env.PORT || 8088;

var server = app.listen(PORT, function () {

    const host = server.address().address;
    const port = server.address().port;

    logger.log('API listening at http://' + host + ':' + port);

    dbConn.getDB(setDBCallback);

});