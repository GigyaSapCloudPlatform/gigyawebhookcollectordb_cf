'use strict';

function log(logTxt) 
{
    console.log("Logger");
    console.log(logTxt);
}

function logJWT(req) {
    var jwt = req.header('authorization');
    if (!jwt) {
        log('No JWT in Request - Call performed directly to App');
        return;
    }
    jwt = jwt.substring('Bearer '.length);
    log('JWT is: ' + jwt);
    xssec.createSecurityContext(jwt, xsenv.getServices({ uaa: 'winterfell-uaa' }).uaa, function(error, securityContext) {
        if (error) {
            log('Security Context creation failed');
            return;
        }
        log('Security Context created successfully');
        var userInfo = {
            logonName : securityContext.getLogonName(),
            giveName :  securityContext.getGivenName(),
            familyName : securityContext.getFamilyName(),
            email : securityContext.getEmail()
        };
        log('User Info retrieved successfully ' + JSON.stringify(userInfo));
    });

    if (req.user) {
        var myUser = JSON.stringify(req.user);
        var myUserAuth = JSON.stringify(req.authInfo);
        log('2nd. XsSec API - user: ' + myUser + ' Security Context: ' + myUserAuth);
    }
    // see it using: cf logs sapcpcfhw --recent
}

module.exports = {
    log: log,
    logJWT: logJWT
};