'use strict';
const xsenv = require('@sap/xsenv');

const DB_service_name = "Kingslanding-DB";

const createTable =
    'CREATE TABLE IF NOT EXISTS public.webhook \
        ( \
            id bigint NOT NULL, \
            nonce varchar(100), \
            status integer,\
            "timestamp" bigint,\
            type varchar(100), \
            uid varchar(100), \
            wid varchar(100), \
            PRIMARY KEY (id) \
        )';

// const dropTable = 'DROP TABLE public.webhook'; // eslint-disable-line no-unused-vars
// const insertRow = "INSERT INTO users(name,color,material) values('SpongeBob','yellow','sponge')"; // eslint-disable-line no-unused-vars

function returnUriToDB() {
    var uri = '';
    if (process.env.VCAP_SERVICES) {
        // running in cloud
        uri = xsenv.cfServiceCredentials(DB_service_name).uri;
    } else {
        console.log('running locally is not supported');
    }
    return uri;
}

function getDB(cb) {
    let pgp = require('pg-promise')({});
    var db = pgp(returnUriToDB());
    let sql = createTable;
    
    db.query(sql)
        .then(function () {
            console.log('database initialized');
            cb(null, db);
            return;
        })
        .catch((err) => {
            console.log(err);
            cb(err, null);
            return;
        });
}

module.exports = {
    getDB: getDB
};

